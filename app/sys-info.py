from flask import Flask, render_template, request
import os
import subprocess

app = Flask(__name__)

si_dir = os.getcwd()
try:
    si_user = os.getlogin()
except Exception:
    si_user = ""
    pass
si_uname_s = os.uname().sysname
si_uname_n = os.uname().nodename
si_uname_r = os.uname().release
si_uname_v = os.uname().version
si_uname_m = os.uname().machine

try:
    with open('/etc/os-release', mode='r') as f:
        si_osrel = {}
        for l in f:
            item = l.split('=', 1)
            si_osrel_update = {item[0]: item[1].rstrip().strip('\"')}
            si_osrel.update(si_osrel_update)
except Exception:
    si_osrel = ""
    pass

try:
    with open('./host/os-release', 'r') as f:
        si_host_osrel = {}
        for l in f:
            item = l.split('=', 1)
            si_host_osrel_update = {item[0]: item[1].rstrip().strip('\"')}
            si_host_osrel.update(si_host_osrel_update)
except Exception:
    si_host_osrel = ""
    pass
try:
    si_host_hostname = open('./host/hostname', 'r').read()
except Exception:
    si_host_hostname = ""
    pass

ip_route_show = subprocess.check_output(
    'ip route show',
    shell=True,
    encoding='UTF-8')
ip_route_list = []
for ip_route_line in ip_route_show.splitlines():
    ip_route_list.append(ip_route_line)

try:
    with open('./host/traceroute-hosts', 'r') as traceroute_hosts:
        traceroute_hosts_list = []
        for traceroute_host in traceroute_hosts:
            traceroute_hosts_list.append(traceroute_host)
except Exception:
    traceroute_hosts_list = []
    traceroute_hosts_list.append('example.com')
    pass

traceroute_lists = []
for traceroute_host in traceroute_hosts_list:
    try:
        traceroute = subprocess.check_output(
            'traceroute ' + traceroute_host,
            shell=True,
            encoding='UTF-8')
    except Exception as e:
        traceroute = "traceroute failed for: " + str(traceroute_host)
        traceroute += str(e)
        pass
    traceroute_list = []
    for traceroute_line in traceroute.splitlines():
        traceroute_list.append(traceroute_line)

    traceroute_lists.append(traceroute_list)

ip_route_show = subprocess.check_output('ip route show', shell=True, encoding='UTF-8')
ip_route_list = []
for ip_route_line in ip_route_show.splitlines():
    ip_route_list.append(ip_route_line)

try:
    with open('./host/traceroute-hosts', 'r') as traceroute_hosts:
        traceroute_hosts_list = []
        for traceroute_host in traceroute_hosts:
            traceroute_hosts_list.append(traceroute_host)
except:
        traceroute_hosts_list = []
        traceroute_hosts_list.append('example.com')
        pass

traceroute_lists = []
for traceroute_host in traceroute_hosts_list:
    try:
        traceroute = subprocess.check_output('traceroute ' + traceroute_host, shell=True, encoding='UTF-8')
    except Exception as e:
        traceroute = "traceroute failed for: " + str(traceroute_host)
        traceroute += str(e)
    traceroute_list = []
    for traceroute_line in traceroute.splitlines():
        traceroute_list.append(traceroute_line)

    traceroute_lists.append(traceroute_list)
    pass


@app.route('/')
def sys_info():
    return render_template(
        './sys-info.html',
        si_dir=si_dir,
        si_iser=si_user,
        si_osrel=si_osrel,
        si_uname_s=si_uname_s,
        si_uname_n=si_uname_n,
        si_uname_r=si_uname_r,
        si_uname_v=si_uname_v,
        si_uname_m=si_uname_m,
        si_host_hostname=si_host_hostname,
        si_host_osrel=si_host_osrel,
        headers=dict(request.headers),
        remote=request.remote_addr,
        referrer=request.referrer,
        access_route=request.access_route,
        ip_route_list=ip_route_list,
        traceroute_lists=traceroute_lists,
        )
