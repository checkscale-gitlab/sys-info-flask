# Use only for test purposes. Flask is not intended to run this way in production. See https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy
FROM python:3.8.0-buster
RUN pip install flask && apt-get update -y && apt-get install traceroute -y && rm -rf /var/lib/apt/lists/*
COPY app /opt/app
WORKDIR /opt/app
EXPOSE 5000
CMD export FLASK_APP=sys-info.py && flask run --host=0.0.0.0
